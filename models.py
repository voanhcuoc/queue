from sqlalchemy import create_engine
from sqlalchemy.types import Time

from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from flask_login import UserMixin
from app import db
from datetime import timedelta, time, datetime

# engine = create_engine('sqlite:///database.db', echo=True)
# db_session = scoped_session(sessionmaker(autocommit=False,
#                                          autoflush=False,
#                                          bind=engine))
# Base = declarative_base()

Base = db.Model
Base.query = db.session.query_property()

# Set your classes here.


class User(Base):
    __tablename__ = 'Users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)
    sdt = db.Column(db.String(20), unique=True)
    password_hash = db.Column(db.String(100))

    def __init__(self, name=None, password_hash=None, email=None, sdt=None):
        self.name = name
        self.password_hash = password_hash
        self.email = email
        self.sdt = sdt

class Service(Base):
    __tablename__ = 'Services'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    next_no = db.Column(db.Integer, default=1)
    serving_no = db.Column(db.Integer, default=0)
    password_hash = db.Column(db.String(100))
    thumbnail_url = db.Column(db.String(120))
    summary = db.Column(db.String(250))
    description = db.Column(db.String(5120))
    name_display = db.Column(db.String(120), unique=True)
    BGURL = db.Column(db.String(512))

    close_time = db.Column(db.Time, default=time(hour=18))
    open_time = db.Column(db.Time, default=time(hour=6))
    estimated_time = db.Column(db.Interval, default=timedelta(minutes=5))


    def __init__(self, name=None, password_hash=None, email=None, sdt=None):
        self.name = name
        self.password_hash = password_hash
        self.email = email
        self.sdt = sdt

import enum
class StatusEnum(enum.Enum):
    waiting = 1
    serving = 2
    finished = 3
    cancelled_user = 4
    cancelled_vendor = 5


class GetNo(Base):
    __tablename__ = 'GetNos'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey(User.id))
    service_id = db.Column(db.Integer, ForeignKey(Service.id))
    no = db.Column(db.Integer)
    status = db.Column(db.Enum(StatusEnum), default = StatusEnum.waiting)
    secret_code = db.Column(db.String(5))
    createdAt = db.Column(db.DateTime, default = datetime.now)
    servedAt = db.Column(db.DateTime)
    finishedAt = db.Column(db.DateTime)

    def __init__(self, user_id=None, service_id=None, no=None):
        self.user_id = user_id
        self.service_id = service_id
        self.no = no

# Create tables.
# Base.metadata.create_all(bind=engine)
# db.create_all()
