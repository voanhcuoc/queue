#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, render_template, request, redirect, url_for, flash, g
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, func
from flask_login import LoginManager, UserMixin, login_user, current_user, login_required, logout_user
import logging
from logging import Formatter, FileHandler
from forms import *
from passlib.hash import pbkdf2_sha256
from datetime import time, datetime, timedelta, date
import os
import string
import random

#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
tz = app.config.get('TIMEZONE')
salt = app.config.get('SECRET_KEY').encode('ascii')

class UserLogin(UserMixin):
    def __init__(self, id, name, type_):
        self.id = id
        self.name = name
        self.type_ = type_
    def get_id(self):
        return '{}:{}'.format(self.type_, self.id)

@app.before_request
def init():
    g.current_user = current_user

# Automatically tear down SQLAlchemy.
'''
@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()
'''

# Login required decorator.
'''
def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login'))
    return wrap
'''

@login_manager.user_loader
def load_user(user_id):
    [type_, id_] = user_id.split(':', 1)
    if type_ == 'user':
        from models import User
        user = db.session.query(User).filter_by(id=id_).first()
    if type_ == 'admin':
        from models import Service
        user = db.session.query(Service).filter_by(id=id_).first()
    if not user:
        return None
    return UserLogin(user.id, user.name, type_)

#----------------------------------------------------------------------------#
# Controllers.
#----------------------------------------------------------------------------#


@app.route('/')
def home():
    if not (current_user and current_user.is_authenticated):
        text = 'Get started'
        link = url_for('user_login')
        bg = 'user'
    elif current_user.type_ == 'user':
        text = "You're logged in. Go to service list."
        link = url_for('service_list')
        bg = 'user'
    else:
        text = 'Go manage your service.'
        link = url_for('admin_startpage')
        bg = 'admin'
    return render_template('pages/home.html', text=text, link=link, bg=bg)


# @app.route('/about')
# def about():
#     return render_template('pages/placeholder.about.html')


@app.route('/login/user', methods = ('GET', 'POST'))
def user_login():
    from models import User

    form = LoginForm()
    form.validate()
    if form.validate_on_submit():
        user = db.session.query(User).filter_by(name = form.name.data).first()
        if user and pbkdf2_sha256.verify(form.password.data, user.password_hash):
            # print('log in an user')
            login_user(UserLogin(user.id, user.name, 'user'))
            flash('logged in as user')
            return redirect(url_for('service_list'))
        else:
            flash('wrong password')
            return redirect(url_for('user_login'))

    return render_template('pages/user_login.html', form=form)


@app.route('/signup/user', methods = ('GET', 'POST'))
def user_signup():
    from models import User

    form = SignupForm()
    form.validate()
    # print(form.errors)
    if form.validate_on_submit():
        # print(form.name.data)
        # print(form.password.data)

        password_hash = pbkdf2_sha256.using(salt=salt).hash(form.password.data)
        user = db.session.query(User).filter_by(name = form.name.data).first()
        # print(user)
        # print(password_hash)
        if not user:
            # print('create new user')
            new_user = User(form.name.data, password_hash)
            db.session.add(new_user)
            db.session.commit()
            # print('sign up an user')
            return redirect(url_for('user_login'))
        else:
            # print('user existed')
            pass
    return render_template('pages/user_signup.html', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/login/admin', methods = ('GET', 'POST'))
def admin_login():
    from models import Service

    form = LoginForm()
    form.validate()
    if form.validate_on_submit():
        user = db.session.query(Service).filter_by(name = form.name.data).first()
        if user and pbkdf2_sha256.using(salt=salt).verify(form.password.data, user.password_hash):
            # print('log in an admin')
            login_user(UserLogin(user.id, user.name, 'admin'))
            flash('logged in as admin')
            return redirect(url_for('admin_startpage'))
        else:
            # flash('wrong password')
            return redirect(url_for('admin_login'))

    return render_template('pages/admin_login.html', form=form)


@app.route('/signup/admin', methods = ('GET', 'POST'))
def admin_signup():
    from models import Service
    form = SignupForm(request.form)

    form.validate()
    # print(form.errors)
    if form.validate_on_submit():
        # print(form.name.data)
        # print(form.password.data)

        password_hash = pbkdf2_sha256.using(salt=salt).hash(form.password.data)
        user = db.session.query(Service).filter_by(name = form.name.data).first()
        # print(user)
        # print(password_hash)
        if not user:
            # print('create new service')
            new_service = Service(form.name.data, password_hash)
            new_service.name_display = form.name.data
            db.session.add(new_service)
            db.session.commit()
            # print('sign up a service')
            return redirect(url_for('admin_login'))
        else:
            # print('service existed')
            pass

    return render_template('pages/admin_signup.html', form=form)

@app.route('/list', methods = ('GET', 'POST'))
@login_required
def service_list():
    from models import Service

    services = db.session.query(Service).all()
    return render_template('pages/service_list.html', services=services)

@app.route('/user/getticket/<int:service_id>')
@login_required
def getticket(service_id):
    if current_user.type_ != 'user':
        return

    from models import Service, GetNo, StatusEnum

    num_ticket = db.session.query(GetNo) \
        .filter_by(service_id=service_id, user_id=current_user.id) \
        .filter(or_(GetNo.status == StatusEnum.waiting, GetNo.status == StatusEnum.serving)).count()

    # print('-------------')
    # print(num_ticket)
    if num_ticket != 0:
        return redirect(url_for('service', id=service_id))

    service = db.session.query(Service).filter_by(id=service_id).first()
    status = StatusEnum.serving if service.next_no == service.serving_no else StatusEnum.waiting
    ticket = GetNo(current_user.id, service_id, service.next_no)
    ticket.status = status
    ticket.secret_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 5)) 
    if status == StatusEnum.serving:
        ticket.servedAt = datetime.now(tz)

    service.next_no += 1
    db.session.add(ticket)
    db.session.add(service)
    db.session.commit()

    # print('==========================')
    # print(ticket)

    return redirect(url_for('ticker', id=ticket.id))

@app.route('/user/ticker/<int:id>', methods = ('GET', 'POST'))
@login_required
def ticker(id):
    if current_user.type_ != 'user':
        return

    from models import Service, GetNo, StatusEnum
    ticket = db.session.query(GetNo) \
        .filter_by(id=id) \
        .first()

    status = ticket.status.__str__().split('.')[1]
    colormap = {
        StatusEnum.waiting: 'orange',
        StatusEnum.serving: 'green',
        StatusEnum.finished: 'grey',
        StatusEnum.cancelled_vendor: 'red',
        StatusEnum.cancelled_user: '#a60394'
    }
    status_color = colormap[ticket.status]
    service = db.session.query(Service).filter_by(id=ticket.service_id).first()
    return render_template('pages/ticket.html', service=service, ticket=ticket, status=status, status_color=status_color)

@app.route('/service/<int:id>', methods = ('GET', 'POST'))
@login_required
def service(id):
    from models import Service, GetNo, StatusEnum

    service = db.session.query(Service).filter_by(id=id).first()
    ticket = db.session.query(GetNo) \
        .filter_by(service_id=id, user_id=current_user.id) \
        .filter(or_(GetNo.status == StatusEnum.waiting, GetNo.status == StatusEnum.serving)).first()

    # print('---------------------------- ')
    # print(ticket)
    # print(service.estimated_time)
    # print(service.close_time)

    closed = False
    now = datetime.now(tz)
    if now.time() < service.open_time or now.time() > service.close_time:
        # print('out of open time')
        closed = True
        expected = None
    elif service.estimated_time:
        if ticket:
            expected = now + (ticket.no - service.serving_no)*service.estimated_time
        else:
            expected = now + (service.next_no - service.serving_no)*service.estimated_time
        if (expected + service.estimated_time).time() > service.close_time:
            closed = True
    else:
        expected = None

    return render_template('pages/service.html', service=service, ticket=ticket, expected=expected, closed=closed)

@app.route('/admin/config', methods = ('GET', 'POST'))
def config():
    from models import Service

    if current_user.type_ != 'admin':
        return

    service = db.session.query(Service).filter_by(id = current_user.id).first()

    form = ServiceConfigForm()
    form.validate()
    # print(form.errors)
    if form.validate_on_submit():
        service.summary = form.summary.data
        service.description = form.description.data
        service.name_display = form.name_display.data
        service.open_time = time(hour = form.open_time_hour.data, minute = form.open_time_minute.data ) 
        service.close_time = time(hour = form.close_time_hour.data, minute = form.close_time_minute.data ) 
        service.estimated_time = timedelta(hours = form.estimated_time_hour.data, minutes = form.estimated_time_minute.data)
        service.BGURL = form.BGURL.data
        service.thumbnail_url = form.thumbnail_url.data
        # print('-------------------------------------')
        # print(service.summary)
        # print(service.description)
        # print(service.name_display)
        # print(service.open_time)
        # print(service.close_time)
        # print(service.estimated_time)
        # print(service.BGURL)
 
        db.session.add(service)
        db.session.commit()

    form.close_time_hour.data = service.close_time.hour
    form.close_time_minute.data = service.close_time.minute
    form.open_time_hour.data = service.open_time.hour
    form.open_time_minute.data = service.open_time.minute
    h = service.estimated_time.seconds
    form.estimated_time_hour.data = h//3600
    form.estimated_time_minute.data = (h%3600)//60


    return render_template('pages/config.html', form=form, service=service)

@app.route('/admin/manage', methods = ('GET', 'POST'))
def manage():
    if current_user.type_ != 'admin':
        return

    from models import Service, GetNo, StatusEnum
    service = db.session.query(Service).filter_by(id=current_user.id).first()
    ticket = db.session.query(GetNo) \
        .filter_by(service_id=service.id) \
        .filter(GetNo.status == StatusEnum.serving).first()
    waiting = service.serving_no < service.next_no

    return render_template('pages/manage.html', service=service, ticket=ticket, waiting=waiting)

@app.route('/admin/next', methods = ('GET', 'POST'))
@login_required
def next():
    if current_user.type_ != 'admin':
        return

    from models import Service, GetNo, StatusEnum
    service = db.session.query(Service).filter_by(id=current_user.id).first()
    if (service.serving_no < service.next_no):
        service.serving_no += 1

        current_ticket = db.session.query(GetNo)\
            .filter_by(service_id=service.id)\
            .filter(GetNo.status == StatusEnum.serving)\
            .first()

        if current_ticket:
            current_ticket.status = StatusEnum.finished
            current_ticket.finishedAt = datetime.now(tz)
            db.session.add(current_ticket)

        next_ticket = db.session.query(GetNo)\
            .filter_by(service_id=service.id, no=service.serving_no)\
            .filter(GetNo.status == StatusEnum.waiting)\
            .first()
        if next_ticket:
            next_ticket.status = StatusEnum.serving
            next_ticket.servedAt = datetime.now(tz)
            db.session.add(next_ticket)

        db.session.add(service)
        db.session.commit()

    return redirect(url_for('manage'))

@app.route('/admin/skip')
def skip():
    if current_user.type_ != 'admin':
        return

    from models import Service, GetNo, StatusEnum
    service = db.session.query(Service).filter_by(id=current_user.id).first()
    current_ticket = db.session.query(GetNo)\
        .filter_by(service_id=service.id)\
        .filter(GetNo.status == StatusEnum.serving)\
        .first()
    if current_ticket:
        current_ticket.status = StatusEnum.cancelled_vendor
        current_ticket.finishedAt = datetime.now(tz)
        db.session.add(current_ticket)
        service.serving_no += 1
        db.session.add(service)
        next_ticket = db.session.query(GetNo)\
            .filter_by(service_id=service.id, no=service.serving_no)\
            .filter(GetNo.status == StatusEnum.waiting)\
            .first()
        if next_ticket:
            next_ticket.status = StatusEnum.serving
            next_ticket.servedAt = datetime.now(tz)
            db.session.add(next_ticket)

    db.session.commit()

    return redirect(url_for('manage'))

@app.route('/admin/reset')
def reset():
    from models import Service
    service = db.session.query(Service).filter_by(id=current_user.id).first()
    service.next_no = 1
    service.serving_no = 0
    db.session.add(service)
    db.session.commit()
    return redirect(url_for('manage'))

@app.route('/admin/history')
@login_required
def history():
    if current_user.type_ != 'admin':
        return
    from models import Service, GetNo, User
    service = db.session.query(Service).filter_by(id=current_user.id).first()
    tickets = db.session.query(GetNo)\
        .filter_by(service_id=current_user.id)\
        .all()

    tickets_today = tickets = db.session.query(GetNo)\
        .filter_by(service_id=current_user.id)\
        .filter(func.date(GetNo.createdAt) == date.today())\
        .all()
    gotTicketHours = list(map(lambda ticket: ticket.createdAt.hour, tickets_today))

    labels = list(range(service.open_time.hour, service.close_time.hour))
    data = list(map(lambda hour: gotTicketHours.count(hour), labels))

    return render_template('pages/history.html', tickets=tickets, labels=labels, data=data)

@app.route('/admin', methods = ('GET', 'POST'))
def admin_startpage():
    if current_user.type_ != 'admin':
        return

    return render_template('pages/admin_startpage.html')



# @app.route('/forgot')
# def forgot():
#     form = ForgotForm(request.form)
#     return render_template('forms/forgot.html', form=form)

# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    #db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#

# Default port:
# if __name__ == '__main__':
#     app.run()

# Or specify port manually:

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)

