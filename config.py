import os
import pytz

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Connect to the database
# postgres
SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgresql://khoa:3ee58e@localhost:5432/queue')
# sqlite
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'trivial.bin') + '?charset=utf8'

# Secret key for session management. You can generate random strings here:
# https://randomkeygen.com/
SECRET_KEY = 'khoa kien kiet'

# Suppress a warning
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Timezone
TIMEZONE = pytz.timezone('Asia/Ho_Chi_Minh')