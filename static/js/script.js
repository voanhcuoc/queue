function drawChart(labels, data) {
    var ctx = document.getElementById('myChart').getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: 'Number of tickets got per hour',
            data: data,
            fill: false,
            borderColor: 'rgb(75, 192, 192)',
            lineTension: 0.1
          }]
        },
        options: {
        }
    });
}