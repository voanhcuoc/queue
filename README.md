# Queue 3K

Online queueing system. See [the plan](plan.pdf) and [the report](report.pdf) for overview.

# Development

```
pip install -r requirements.txt
python init_default.py
python app.py
```

Dependencies:

* `Flask` web framework.
* `Flask-Login` for login management.
* `Flask-WTF` for form parsing and validation.
* `Flask-SQLAlchemy` and `psycopg2` for database integration.
* `passlib` for password hashing (SHA256).
* `gunicorn` for server deployment.
* Bootstrap 4 for web styling.

Modules:

* `app.py`: all views and business logic.
* `config.py`: all configurations.
* `forms.py`: all form models.
* `models.py`: all database models.
* `init_default.py`: initialize sane defaults (look at module code for contents).

# Deployment

To Heroku:

```
heroku create <subdomain>
heroku addons:create heroku-postgresql:hobby-dev
git push heroku master
heroku run python init_default.py
```
