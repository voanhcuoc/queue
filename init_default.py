from models import db, User, Service, GetNo
from passlib.hash import pbkdf2_sha256
from datetime import time, timedelta
from config import SECRET_KEY

salt=SECRET_KEY.encode('ascii')

db.create_all()

khoa = User('chetiepchankhoa', pbkdf2_sha256.using(salt=salt).hash('12345678'), 'ch@nkhoa.email', '0123456789')
kiet = User('kiet123', pbkdf2_sha256.using(salt=salt).hash('12345678'))
kien = User('kien123', pbkdf2_sha256.using(salt=salt).hash('12345678'))
db.session.add(khoa)
db.session.add(kiet)
db.session.add(kien)

def add_service(name, password, name_display, summary, thumbnail_url, BGURL, description, close_time, estimated_time, open_time=time(hour=6)):
	service = Service(name, pbkdf2_sha256.using(salt=salt).hash(password))
	service.name_display = name_display
	service.summary = summary
	service.thumbnail_url = thumbnail_url
	service.BGURL = BGURL
	service.description = description
	service.close_time = close_time # 5pm
	service.estimated_time = estimated_time
	service.open_time = open_time
	db.session.add(service)

add_service('Circle K', '12345678', 'Circle K cổng 2', '525 Tô Hiến Thành, Phường 14, Quận 10, Hồ Chí Minh',
'/static/img/kiet/circlek.png','/static/img/kiet/circlekbg.jpg','''Khuyến mãi đặc biệt tháng 07/2020 áp dụng trên toàn quốc
Từ 01.07.2020 đến 28.07.2020
Đến Circle K - chuỗi cửa hàng tiện lợi mở cửa 24/7 để trải nghiệm phong cách mua sắm tiện lợi, nhanh chóng với những ưu đãi hấp dẫn không thể bỏ lỡ!
''',time(5 + 12),timedelta(minutes=5))

add_service('Cybercore', '12345678', 'Cybercore N3', 'N3 Trường Sơn, Cư xá Bắc Hải, P.15, Q.10, Ho Chi Minh','/static/img/kiet/cyber.png','/static/img/kiet/cyberbg.jpg','''CyberCore Premium là chuỗi phòng máy đầu tiên dưới sự đầu tư của một nữ streamer Uyên Pu cực kì nổi tiếng trong làng Streamer Việt. 
Tại đây cái gì cũng có, Từ phòng Couple cho tới quán trà sữa, từ dịch vụ cho tới không gian, như một trung tâm dịch vụ giải trí thứ thiệt, tất cả đều có thể giúp các bạn vui vẻ và thoải mái khi đến đây. 
''',time(5 + 12),timedelta(minutes=7))


add_service('7-Eleven', '12345678', '7-Eleven', '412 Nguyễn Thị Minh Khai, Phường 5, Quận 3, TP. Hồ Chí Minh','/static/img/kiet/7.png','/static/img/kiet/7bg.jpg','''Cửa hàng tiện lợi 7-Eleven đầu tiên ở Việt Nam được mở vào năm 2017, đưa Việt Nam trở thành quốc gia thứ 19 góp mặt trong hệ thống chuỗi cửa hàng tiện lợi lớn nhất thế giới này.

Sự hiện diện của 7-Eleven tại Việt Nam được thực hiện thông qua nhượng quyền độc quyền cho Công ty Cổ phần Seven System Việt Nam (SSV) – là nơi tập hợp đội ngũ trẻ và năng động đang không ngừng nỗ lực để tiếp nối thành công mà thương hiệu này đã xác lập trên thế giới. 
''',time(5 + 12),timedelta(minutes=10))

db.session.commit()