from flask_wtf import FlaskForm as Form
from wtforms import TextField, PasswordField, SelectField
from wtforms.validators import DataRequired, EqualTo, Length

# Set your classes here.


class SignupForm(Form):
    name = TextField(
        'Username', validators=[DataRequired(), Length(min=6, max=25)]
    )
    # email = TextField(
    #     'Email', validators=[DataRequired(), Length(min=6, max=40)]
    # )
    password = PasswordField(
        'Password', validators=[DataRequired(), Length(min=6, max=40)]
    )
    confirm = PasswordField(
        'Repeat Password',
        [DataRequired(),
        EqualTo('password', message='Passwords must match')]
    )


class LoginForm(Form):
    name = TextField('Username', [DataRequired()])
    password = PasswordField('Password', [DataRequired()])


class ForgotForm(Form):
    email = TextField(
        'Email', validators=[DataRequired(), Length(min=6, max=40)]
    )

# [workaround]
# https://github.com/wtforms/wtforms/pull/598
def dupzip2list(i):
    l = list(i)
    return list(zip(l, l))

class ServiceConfigForm(Form):
    summary = TextField(
        'Summary', validators=[Length(min=0, max=512)]
    )
    description = TextField(
        'Description', validators=[Length(min=0, max=512)]
    )
    name_display = TextField(
        'Description', validators=[Length(min=0, max=120)]
    )
    BGURL = TextField(
        'Background URL', validators=[Length(min=0, max=512)]
    )
    thumbnail_url = TextField(
        'Thumbnail URL', validators=[Length(min=0, max=512)]
    )
    open_time_hour = SelectField(
        'Open Time' , choices=dupzip2list(range(24)), coerce=int
    )
    open_time_minute = SelectField(
        'Open Time' , choices=dupzip2list(range(60)), coerce=int
    )
    close_time_hour = SelectField(
        'Close Time' , choices=dupzip2list(range(24)), coerce=int
    )
    close_time_minute = SelectField(
        'Close Time' , choices=dupzip2list(range(60)), coerce=int
    )
    estimated_time_hour = SelectField(
        'Estimated Time' , choices=dupzip2list(range(24)), coerce=int
    )
    estimated_time_minute = SelectField(
        'Estimated Time' , choices=dupzip2list(range(60)), coerce=int
    )


